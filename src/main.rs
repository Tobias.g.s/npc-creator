use clap::Parser;
use rand::Rng;

mod name;

#[derive(Parser)]
struct Cli {
    number_of_npcs:u32,
}

fn generate_npc() -> String{
    let races = vec![
        "🧑", "🤖", "🐀", "🪲", "🦎", 
    ];

    let classes = vec![
        "Soldier","Envoy","Operative",
        "Mechanic","Mystic","Technomancer",
        "Solarian","Biohacker","Vangaurd","Witchwarper",
    ];

    let themes = vec![
        "AcePilot","BountyHunter","Icon","Mercenary",
        "Outlaw","Priest","Scholar","Spacefarer",
        "Xenoseeker","Themeless",
    ];

    let traits = vec![
        "With an adventurous spirit", "Full of compassion", "Noted for their diligence",
        "Always optimistic", "an example of resilience", "With a charismatic personality",
        "Noted for their creativity", "known for displaying patience", "Known for their loyalty",
        "Filled with curiosity", "know to be arrogant", "with a tendency to be impulsive",
        "Often pessimistic", "Known for being vindictive", "Sometimes jealous",
        "Stubborn at times", "Known for being irresponsible", "Can be selfish",
        "Known to be dishonest", "Unpredictably unpredictable", "Eccentric in nature",
        "With a mysterious aura", "Noted for being obsessive", "Reserved in nature",
        "Skeptical at times", "Often lost in dreams", "A perfectionist to a fault",
        "Competitively competitive", "Methodical in approach", "Known for quirky their behavior"
    ];

    let mut random_number_generator = rand::thread_rng();

    let class = classes[random_number_generator.gen_range(0..classes.len())];
    let character_trait = traits[random_number_generator.gen_range(0..traits.len())];
    let theme = themes[random_number_generator.gen_range(0..themes.len())];
    let race = races[random_number_generator.gen_range(0..races.len())];
    let name = name::generate_name(race);
    format!("Welcome {} {}, the {} {}, {}", name , race, class, theme, character_trait)
}


fn main() {
    for _ in 0..5{
        println!("{}", generate_npc())
    }
}
