use rand::Rng;


pub fn generate_name(race: &str) -> String {
    let mut syllables = vec![];
    //  "🧑", "🤖", "🐀", "🪲", "🦎", 
    match race {
        "🤖" => syllables = vec!["Al", "Be", "Tron", "Cy", "Pho", "Gra",
                                        "Me", "Tri", "Xy", "Zet", "Uni", "Di",
                                        "Git", "Tal", "Ax", "Lux"],

        "🧑" => syllables = vec!["Kes", "Ka", "Sa", "Tha", "Ze", "Vex",
                                        "Dran", "Rak", "Tho", "Dai", "Drin", 
                                        "Chko", "Resk", "Trak", "Lor"],

        "🪲" => syllables = vec!["La", "Shun", "Ta", "Mel", "Dia", "Re",
                                        "Sa", "Nal", "Qi", "Vi", "Phan", "Ki",
                                        "Yso", "Var", "Ros", "Sae"],

        "🦎" => syllables = vec!["Vesk", "Har", "Sek", "Rok", "Tok", "Gnak",
                                    "Zol", "Tresk", "Sk", "Gno", "Kres", "Za"],

        "🐀" => syllables = vec!["Yk", "Ris", "Nib", "Giz", "Yso", "Ki", "Brix",
                                    "Dol", "Zi", "Vik", "Rik", "Zix"],

                                    _ => (),
    }

    let mut random_number_generator = rand::thread_rng();
    let name_length = random_number_generator.gen_range(2..5);
    let mut name = String::new();

    for i in 0..name_length {
        let index = random_number_generator.gen_range(0..syllables.len());
        if i == 0 {
            name.push_str(&syllables[index]);
        } else {
            name.push_str(&syllables[index].to_lowercase());
        }
    }
    name
}

